/* Demonstrates that QuickControls2 Dialog's don't support modality
 *
 */
import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    DialogWithQC2 {
        id: dialogThatShouldBeModal
    }

    DialogWithQC1 {
        id: dialogThatIsModal
    }

    Text {
        id: explanation
        text: "Demonstrates that QtQuick.Controls 2.1 dialogs aren't modal";
    }

    Button {
        id: button1
        onClicked: dialogThatShouldBeModal.open()
        x: 0
        y: explanation.height + 10
        text: "Show me a dialog that SHOULD be modal"
    }

    Button {
        onClicked: dialogThatIsModal.open()
        x: 0
        y: button1.height + 40
        text: "Show me a REAL modal dialog"
    }
}
