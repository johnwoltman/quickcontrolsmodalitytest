import QtQuick 2.4
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2

Dialog {
    modality: Qt.WindowModal
    Rectangle {

        width: 500
        height: 200
        border.width: 1
        color: "yellow"
        Text {
            text: "I am modal"
        }
        Button {
            y: 30
            text: "Close me"
            onClicked: dialogThatIsModal.close()
        }
    }
}
