import QtQuick 2.4
import QtQuick.Controls 2.1

Dialog {
    modal: true
    Rectangle {

        width: 500
        height: 200
        border.width: 1
        color: "yellow"
        Text {
            text: "I want to be modal, but if you click in the gray area I'll disappear :-("
        }
        Button {
            y: 30
            text: "Close me"
            onClicked: dialogThatShouldBeModal.close()
        }
    }
}
